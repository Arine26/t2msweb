import { Component, OnInit, Input } from '@angular/core';
import {get} from '../data/get.component'
import { Posts} from "../data/post.component";
import { Api } from "../server/Api"


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  @Input() getsData:get;
  oposts:Posts;
  id:string;
  
  
  constructor(private _Api:Api) { }

  ngOnInit() {
    this.oposts = new Posts();
    if(this.getsData!= null || this.getsData != undefined){
      this.oposts.name = this.getsData.name;
      this.oposts.Email_id = this.getsData.Email_id;
      this.oposts.password = this.getsData.password;
      this.oposts.phone_no = this.getsData.phone_no;
      this.id = this.getsData.id;
    }
  }
  onClickSubmit(){
    console.log(this.oposts);
    if(this.getsData == null || this.getsData == undefined){
      this._Api.postData(this.oposts)
      .subscribe(
        data =>{
          console.log(data);
        }
      );
    }
    else{
      // this._Api.update(this.oposts,this.id)
      // .subscribe(
      //   data=>
      //   {

      //   }
      // )
    }
    
    }


    closeComponent(){

    }
}
