import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Api} from './server/Api';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SlideBarComponent } from './slide-bar/slide-bar.component';
import { LayoutComponent } from './layout/layout.component';
import { SignupComponent } from './signup/signup.component';
import {HttpClientModule} from'@angular/common/http';
import {FormsModule} from '@angular/forms';
import { from } from 'rxjs';
import { LoginComponent } from './login/login.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SlideBarComponent,
    LayoutComponent,
    SignupComponent,
    LoginComponent,
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [Api],
  bootstrap: [AppComponent]
})
export class AppModule { }
