import { Component, OnInit } from '@angular/core';
import { Api } from '../server/Api';
import {get} from '../data/get.component';
import {Posts} from '../data/post.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(private _Api:Api) { }
  lstget:get[];
  isOpenSignUp:boolean;
  oposts:Posts;
  editData:get;


  ngOnInit() {
    this._Api.getData()
    .subscribe
    (
      data=>
      {
        this.lstget=data;
      }
    )  
    this.oposts = new Posts();
  };
  openComponent(){
    this.isOpenSignUp =true;
  }
}
  


