 import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http'
import { Posts } from '../data/post.component';


const httpOption={
  headers:new HttpHeaders({
      'Content-type':'application/json'
    })
};


@Injectable()
 export class Api{
   constructor(private httpClient:HttpClient){

   }
    getData():Observable<any>{

      console.log(this.httpClient.get("http://192.168.43.72:5005/user"))
      // return this.httpClient.get("http://192.168.43.72:5005/user")  //Home
      return this.httpClient.get("http://192.168.0.146:5005/user") //office
    }

    postData(postsData:Posts):Observable<any>{
      // return this.httpClient.post("http://192.168.43.72:5005/signup",postData); //Home
      return this.httpClient.post("http://192.168.0.146:5005/signup",postsData); //office
  }
  update(updateData:Posts,id:string):Observable<any>{
    return this.httpClient.put("http://dummy.restapiexample.com/api/v1/update/"+id,updateData)
}
 }
